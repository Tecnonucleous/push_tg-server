import httpclient, ../ecec/ece

proc encrypt*(msg: var string, p256dh: var uint8, p256dh_len: int, auth: var uint8, auth_len: int): string =
    var payload: uint8
    let payload_len = ece_aes128gcm_payload_max_length(ECE_WEBPUSH_DEFAULT_RS, 0, msg.len)
    let err = ece_webpush_aes128gcm_encrypt(p256dh.addr, p256dh_len, auth.addr, auth_len, ECE_WEBPUSH_DEFAULT_RS, 0, cast[ptr uint8](msg.addr), msg.len, payload.addr, payload_len.unsafeAddr)

proc send_push*(endpoint: string, encripted_msg: string): string =
    let client = newHttpClient()
    client.headers = newHttpHeaders({ "Content-Encoding": "aes128gcm" })
    let response = client.request(endpoint, httpMethod = HttpPost, body = encripted_msg)
    return response.status
