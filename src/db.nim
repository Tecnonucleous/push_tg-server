import asyncdispatch, asyncmysql, logging, strutils, sequtils, re
export asyncmysql.AsyncMysqlPool

type
    Reply* = tuple[packet: ResultPacket, rows: seq[string]]

proc fix_utf8(str: string): string =
    var i = 0
    while true:
        var (s,e) = str.findBounds(re"\b\\x[0-9A-F]{2}\b", i)
        if i == 0: 
            result = str.substr(0, s-1)
        if s < 0:
            result &= str.substr(if i == 0: 0 else: i+1, str.len()-1)
            break
        i = e
        result &= str.substr(s+2, e).parseHexStr

proc start_db*(host = "127.0.0.1", port = Port(3306), user = "mysql", pass = "123", db = "pushtg_server"): Future[AsyncMysqlPool] =
    return openMysqlPool( 
                    port=port, 
                    host=host, 
                    user=user, 
                    password=pass, 
                    database=db)

proc send_query(pool: AsyncMysqlPool, q: SqlQuery): Future[seq[Reply]] =
    var retFuture = newFuture[seq[Reply]]("send_query")
    result = retFuture

    proc finishCb(err: ref Exception, replies: seq[Reply]) {.async.} =
        if err == nil:
            retFuture.complete(replies)
        else:
            retFuture.fail(err)

    pool.execQuery(q, finishCb)

template make_query[T](pool: AsyncMysqlPool, query: string, args: varargs[string, `$`], result_action: proc(r: seq[Reply], f: Future[T], query: string)) =
    let r = waitFor pool.send_query(sql(query, args))
    var retFuture = newFuture[T](query)
    result = retFuture

    result_action(r, retFuture, sql(query, args).string)

proc bool_q(r: seq[Reply], f: Future[bool], query: string) =
    if r[0].packet.kind == rpkOk:
        f.complete(true)
    else:
        echo "Error on " & query & ": " & $r
        fatal("Error on " & query & ": " & $r)
        f.complete(false)

proc bool_sq(r: seq[Reply], f: Future[bool], query: string) =
    if r[0].packet.kind == rpkResultSet and r[0].rows.len() > 0:
        f.complete(true)
    else:
        f.complete(false)

proc add_tosend*(pool: AsyncMysqlPool, id: int): Future[bool] =
    pool.make_query("INSERT INTO `to_send` (`id`) VALUES (?)", id, bool_q)

proc exist_tosend*(pool: AsyncMysqlPool, id: int): Future[bool] =
    pool.make_query("SELECT `id` FROM `to_send` WHERE id = ?", id, bool_sq)

proc set_active_tosend*(pool: AsyncMysqlPool, id: int, active: bool): Future[bool] =
    pool.make_query("UPDATE `to_send` SET `active` = ? WHERE `to_send`.`id` = ?", if active: 1 else: 0, id, bool_q)

proc get_active_tosend*(pool: AsyncMysqlPool): Future[seq[tuple[id: int, total_send: int, last_send: string, added: string]]] =
    let r = waitFor pool.send_query(sql("SELECT * FROM `to_send` WHERE `active` = 1"))
    #let r = waitFor pool.make_query("INSERT INTO `to_send` (`id`) VALUES ('?')", id)
    var retFuture = newFuture[seq[tuple[id: int, total_send: int, last_send: string, added: string]]]("get_a_tosend")
    result = retFuture

    if r[0].packet.kind == rpkError:
        retFuture.fail(newException(IOError, r[0].packet.errorMessage))
    else:
        var re: seq[tuple[id: int, total_send: int, last_send: string, added: string]] = @[]
        if r[0].packet.kind == rpkResultSet and r[0].rows.len() > 0:
            for d in r[0].rows.distribute((r[0].rows.len/5).toInt):
                re.add((d[0].parseInt(), d[2].parseInt, d[3], d[4]))
        retFuture.complete(re)

proc update_tosend*(pool: AsyncMysqlPool): Future[bool] =
    pool.make_query("UPDATE `to_send` SET `total_send`=`total_send`+1,`last_send`=CURRENT_TIMESTAMP WHERE `active` = 1", "", bool_q)

proc add_article*(pool: AsyncMysqlPool, title: string, img: string, url: string): Future[bool] =
    pool.make_query("INSERT INTO `articles` (`title`, `img`, `url`) VALUES (?, ?, ?)", title, img, url, bool_q)

proc search_article*(pool: AsyncMysqlPool, id: int = -1, url: string = ""): Future[seq[tuple[id: int, title: string, img: string, url: string]]] =
    let val = if id > 0: id.intToStr() else: url
    let r = waitFor pool.send_query(sql("SELECT * FROM `articles` WHERE" & (if id > 0: " `id` = ?" else: "`url` LIKE ?"), val))
    #let r = waitFor pool.make_query("INSERT INTO `to_send` (`id`) VALUES ('?')", id)
    var retFuture = newFuture[seq[tuple[id: int, title: string, img: string, url: string]]]("search_article")
    result = retFuture

    if r[0].packet.kind == rpkError:
        retFuture.fail(newException(IOError, r[0].packet.errorMessage))
    else:
        var re: seq[tuple[id: int, title: string, img: string, url: string]] = @[]
        if r[0].packet.kind == rpkResultSet and r[0].rows.len() > 0:
            for row in r[0].rows.distribute((r[0].rows.len/4).toInt):
                re.add((row[0].parseInt(), row[1].fix_utf8(), row[2], row[3]))
        retFuture.complete(re)

proc get_last_art*(pool: AsyncMysqlPool): Future[string] =
    let r = waitFor pool.send_query(sql("SELECT id FROM articles ORDER BY id DESC LIMIT 1"))
    var retFuture = newFuture[string]("get_last_art")
    result = retFuture

    if r[0].packet.kind == rpkError or r[0].rows.len() < 0:
        retFuture.fail(newException(IOError, r[0].packet.errorMessage))
    else:
        retFuture.complete(r[0].rows[0])

proc add_msg*(pool: AsyncMysqlPool, msg_id: int, chat_id: int): Future[bool] =
    let r = waitFor pool.send_query(sql("INSERT INTO `messages` (`msg_id`, `chat_id`, `art_id`) VALUES (?, ?, (SELECT id FROM articles ORDER BY id DESC LIMIT 1))", msg_id, chat_id))
    #let r = waitFor pool.make_query("INSERT INTO `to_send` (`id`) VALUES ('?')", id)
    var retFuture = newFuture[bool]("add_msg")
    result = retFuture

    if r[0].packet.kind == rpkOk:
        retFuture.complete(true)
    else:
        echo("error addind msg: " & $msg_id & $chat_id & ". " & $r)
        retFuture.complete(false)

proc get_msg*(pool: AsyncMysqlPool, msg_id: int, chat_id: int): Future[seq[int]] =
    let r = waitFor pool.send_query(sql("SELECT `art_id` FROM `messages` WHERE `msg_id` = ? AND `chat_id` = ?", msg_id, chat_id))
    var retFuture = newFuture[seq[int]]("get_msg")
    result = retFuture

    if r[0].packet.kind == rpkError:
        retFuture.fail(newException(IOError, r[0].packet.errorMessage))
    else:
        var re: seq[int] = @[]
        if r[0].packet.kind == rpkResultSet and r[0].rows.len() > 0:
            for row in r[0].rows:
                re.add(row.parseInt())
        retFuture.complete(re)

proc add_comments*(pool: AsyncMysqlPool, msg_id: int, user_id: int, chat_id: int, art_id: int, text: string, img: string): Future[bool] =
    pool.make_query("INSERT INTO `comments` (`msg_id`, `user_id`, `art_id`, `text`, `img`) VALUES (?, ?, ?, ?, ?)", msg_id, user_id, chat_id, art_id, text, img, bool_q)

proc add_user*(pool: AsyncMysqlPool, id: int, name: string, accept_msg: int = -1): Future[bool] =
    pool.make_query(if accept_msg > 0: "INSERT INTO `users` (`id`, `name`) VALUES (?, ?)" else: "INSERT INTO `users` (`id`, `name`, `accept_msg`, `accept_date`) VALUES (?, ?, ?, CURRENT_TIMESTAMP)", id, name, accept_msg, bool_q)

proc exist_user*(pool: AsyncMysqlPool, id: int): Future[bool] =
    pool.make_query("SELECT `id` FROM `users` WHERE id = ?", id, bool_sq)

proc add_accept*(pool: AsyncMysqlPool, user_id: int, id: int): Future[bool] =
    pool.make_query("UPDATE `users` SET `accept_msg` = ?,`accept_date` = CURRENT_TIMESTAMP WHERE `id` = ?", id, user_id, bool_q)

proc exist_accept*(pool: AsyncMysqlPool, user_id: int, id: int = 0): Future[bool] =
    pool.make_query(if id > 0: "SELECT `accept_msg` FROM `users` WHERE id = ? AND accept_msg = ?" else: "SELECT `accept_msg` FROM `users` WHERE id = ?", user_id, id, bool_sq)

proc add_push_stats*(pool: AsyncMysqlPool, platform: string, product: string, useragent: string): Future[bool] =
    pool.make_query("INSERT INTO `push_stats` (`platform`, `product`, `useragent`) VALUES (?, ?, ?)", platform, product, useragent, bool_q)

proc add_timing_stats*(pool: AsyncMysqlPool, connection: int64, domain: int64, request: int64, response: int64, parse: int64, content: int64, total_load: int64): Future[bool] =
    pool.make_query("INSERT INTO `timing_stats` (`connection`, `domain`, `request`, `response`, `parse`, `content`, `total_load`) VALUES (?, ?, ?, ?, ?, ?, ?)", connection, domain, request, response, parse, content, total_load, bool_q)

proc add_push_subscription*(pool: AsyncMysqlPool, endpoint: string, id: string, p256dh: int, auth: int, plen: int, alen: int): Future[bool] =
    pool.make_query("INSERT INTO `push_subscription` (`endpoint`, `id`, `p256dh`, `auth`, `plen`, `alen`) VALUES (?, ?, ?, ?, ?, ?)", endpoint, id, p256dh, auth, plen, alen, bool_q)

proc get_push_subscriptions*(pool: AsyncMysqlPool): Future[seq[tuple[endpoint: string, id: string, p256dh: uint8, auth: uint8, plen: int, alen: int]]] =
    let r = waitFor pool.send_query(sql("SELECT * FROM `to_send` WHERE `active` = 1"))
    var retFuture = newFuture[seq[tuple[endpoint: string, id: string, p256dh: uint8, auth: uint8, plen: int, alen: int]]]("get_pushs")
    result = retFuture

    if r[0].packet.kind == rpkError:
        retFuture.fail(newException(IOError, r[0].packet.errorMessage))
    else:
        var re: seq[tuple[endpoint: string, id: string, p256dh: uint8, auth: uint8, plen: int, alen: int]] = @[]
        if r[0].packet.kind == rpkResultSet and r[0].rows.len() > 0:
            for d in r[0].rows.distribute((r[0].rows.len/4).toInt):
                re.add((d[0], d[1], d[2].parseInt.uint8, d[3].parseInt.uint8, d[4].parseInt, d[5].parseInt))
        retFuture.complete(re)