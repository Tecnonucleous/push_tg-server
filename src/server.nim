import httpbeast, telebot,
    options, threadpool, asyncdispatch, logging, json,
    tg_handlers, webhook_msg, push, db
from times import getTime, toUnix, Time


proc init_log() =
    var l = newConsoleLogger()
    addHandler(l)

proc start_webserver(db_conf: tuple[host: string, user: string, pass: string], bot: TeleBot, port: int, key1: string, key2: string, push_endpoint: string) =
    init_log()
    var db = waitFor start_db(host = db_conf.host, user = db_conf.user, pass = db_conf.pass)

    proc onRequest(req: Request): Future[void] {.gcsafe.} =
        echo req.httpMethod.get()
        echo req.path.get()
        if req.httpMethod == some(HttpPost):
            if req.headers.get()["Content-Type"].contains("application/json"):
                if req.path.get() == "/" & key1 & "/" & key2 or req.path.get() == "/" & key1 & "/" & key2 & "/":
                    try:
                        let body = parsejson(req.body.get());
                        let msg = newWebhook_msg(body["text"].getStr(), body["icon_url"].getStr)
                        if msg.post_url.isNil:
                            for g in waitFor db.get_active_tosend():
                                bot.send(g.id.newMessage(msg.text))
                        else:
                            discard waitFor db.add_article(msg.post_title, msg.post_img, msg.post_url)
                        
                            for g in waitFor db.get_active_tosend():
                                let msg_id = waitFor g.id.tg_send(bot, waitFor db.get_last_art(), msg)
                                discard waitFor db.add_msg(msg_id.messageId, msg_id.chat.id)
                            discard waitFor db.update_tosend()

                            for sub in waitFor db.get_push_subscriptions():
                                var pmsg = $ %* {"title": msg.post_title, "icon": msg.icon, "img": msg.post_img, "body": msg.post_description}
                                var p256dh = sub.p256dh
                                var auth = sub.auth
                                var encripted_msg: string = pmsg.encrypt(p256dh, sub.plen, auth, sub.alen)
                                echo sub.endpoint.send_push(encripted_msg)
                    except:
                        req.send(Http400)
                elif req.path.get() == "/" & push_endpoint or req.path.get() == "/" & push_endpoint & "/":
                    try:
                        let body = parsejson(req.body.get());
                        
                        let sub = body["sub"]
                        discard waitFor db.add_push_subscription(sub["endpoint"].getStr(), sub["id"].getStr(), sub["p256dh"].getInt(), sub["auth"].getInt(), sub["p256_leng"].getInt(), sub["auth_leng"].getInt())

                        let stats = body["stats"]
                        #TODO: timings and lang
                        discard waitFor db.add_push_stats(stats["platforms"].getStr(), stats["product"].getStr(), stats["useragent"].getStr())
                        #discard waitFor db.add_timing_stats()
                    except:
                        req.send(Http400)
                else:
                    req.send(Http403)
            else:
                req.send(Http404)
        else:
            req.send(Http404)

    while true:
        try:
            run(onRequest, initSettings(Port(port)))
        except:
            error "Error on Webserver: " & getCurrentExceptionMsg()


proc start_tgbot(db: AsyncMysqlPool, bot: TeleBot, priv_channel: string, timeout: int) =
    bot.onCommand("start", on_tg_start(db))
    bot.onCommand("comentario", on_tg_allow_c)
    #bot.onCommand("respuesta", on_tg_allow_rc(db))
    bot.onCommand("activar_noticias", on_tg_disable_news(db, false))
    bot.onCommand("desactivar_noticias", on_tg_disable_news(db))
    bot.onInlineQuery(tg_inline(db))
    bot.onUpdate(tg_update(priv_channel, db))
    discard bot.set_vars()

    while true:
        try:
            bot.poll(timeout.int32)
        except:
            error "Error on telegram bot: " & getCurrentExceptionMsg()

proc start(config_file = "config.json") =
    #Start the server with the config file
    init_log()
    var config: JsonNode
    try:
        config = parseFile(config_file)
    except JsonParsingError:
        fatal("Error parsing the config file.")
        return
    except IOError:
        fatal("Config file not found.")
        return

    let bot = newTeleBot(config["tg_api_key"].getStr())
    var db = waitFor start_db(
        host = config["mysql_host"].getStr(),
        user = config["mysql_user"].getStr(),
        pass = config["mysql_pass"].getStr()
    )

    spawn start_webserver((config["mysql_host"].getStr(), config["mysql_user"].getStr(), config["mysql_pass"].getStr()), bot, config["web_port"].getInt(), config["webhook_key1"].getStr(), config["webhook_key2"].getStr(), config["push_endpoint"].getStr())
    start_tgbot(db, bot, config["tg_channel"].getStr(), config["tg_timeout"].getInt())

when isMainModule:
    import cligen; dispatch(start)