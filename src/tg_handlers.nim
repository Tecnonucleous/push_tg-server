import telebot, db, asyncdispatch, strutils, options, json, sequtils, tables, webhook_msg, unidecode, nre
from times import getTime, toUnix, Time

const
    ACCEPT = "acepto los terminos y condiciones y politica de privacidad"
    SHORT_ACCEPT = "Acepto"
    DECLINE = "rechazo los terminos y condiciones y politica de privacidad"

    ACCEPT_THE_TOS = "Por favor, para publicar su comentario necesitamos que aceptes nuestros condiciones y términos de servicio y las políticas de privacidad."

    ACCEPTED = "Has aceptado las condiciones y términos de servicio y las políticas de privacidad de Tecnonucleous, por lo que tus comentarios serán mostrados en la web.\n Puedes cambiar esto en cualquier momento enviando /comentario y dándole a rechazar."
    DECLINED = "Has rechazado las condiciones y términos de servicio y las políticas de privacidad de Tecnonucleous, por lo que tus comentarios no serán mostrados en la web.\n Puedes cambiar esto en cualquier momento enviando /comentario y dándole a aceptar."
    TOS = "Para poder mostrar sus comentarios en la web, necesitamos que acepte nuestros [Términos y condiciones de servicio y política de privacidad](https://privacidad.tecnonucleous.com/#bot).\n Use el teclado de abajo para aceptarlas o rechazarlas."

    ONLY_PRIVATE = "Este comando solo funciona en chat privado.\n Si desea aceptar respuestas como comentarios en su grupo, use /respuesta"
    ONLY_PUBLIC = "Este comando solo funciona en chats de grupo o canales."

    DISABLE_NEWS = "Has activado la recepcion de noticias."
    ENABLE_NEWS = "Has activado la recepcion de noticias."

    COMMENT = "Comentar"
    SHARE = "Compartir"
    SEND_COMMENT = "Por favor, escriba su comentario y luego envie el mensaje respondiendo a este."

    INSERT_URL = "Por favor, introduzca una url valida."
    INVALID_URL = "La URL no se encuentra indexada. Pronto arreglaremos este error."

var to_comment: Table[int, int] = initTable[int, int](16)
var cache_me: User

proc gen_accept_key(): KeyboardMarkup =
    var accept_btn = initInlineKeyBoardButton(SHORT_ACCEPT)
    accept_btn.url = option("t.me/tecnotest_bot?start=accept")

    return newInlineKeyboardMarkup(@[@[accept_btn]])

proc gen_art_key(id: string, url: string): KeyboardMarkup =
    var comment_btn = initInlineKeyBoardButton(COMMENT)
    var share_btn = initInlineKeyBoardButton(SHARE)

    comment_btn.url = option("t.me/tecnotest_bot?start=" & id)
    share_btn.switchInlineQuery = option(url)
    return newInlineKeyboardMarkup(@[@[comment_btn, share_btn]])

proc check_public_changes(db: AsyncMysqlPool, bot: TeleBot, msg: Message) {.async.} =
    if not await db.exist_tosend(msg.chat.id):
        discard await db.add_tosend(msg.chat.id)
            
    if msg.leftChatMember.isSome() and msg.leftChatMember.get.isBot and msg.leftChatMember.get.id == cache_me.id:
        discard await db.set_active_tosend(msg.chat.id, false)
    elif msg.newChatMembers.isSome() and msg.newChatMembers.get.contains(cache_me):
        #TODO: Posibily this doesnt work
        discard await db.set_active_tosend(msg.chat.id, true)

proc check_comments(db: AsyncMysqlPool, bot: TeleBot, msg: Message) {.async.} =
    if msg.replyToMessage.isSome() and msg.replyToMessage.get.fromUser.isSome() and msg.replyToMessage.get.fromUser.get.id == cache_me.id:
        if msg.fromUser.isSome(): # and db[msg.chat.id.intToStr()]["allowc"]
            let msg_notice = await db.get_msg(msg.replyToMessage.get.messageId, msg.chat.id)
            if msg_notice.len() > 0:
                if not await db.exist_accept(msg.fromUser.get.id):
                    var send_msg = msg.chat.id.newMessage(ACCEPT_THE_TOS)
                    send_msg.replyToMessageId = msg.messageId
                    send_msg.replyMarkup = gen_accept_key()
                    bot.send(send_msg)

                #TODO: videoburbuja, stikers, ...
                discard await db.add_comments(msg.messageId, msg.fromUser.get.id, msg.chat.id, msg_notice[0], if msg.text.isSome: msg.text.get else: "", if msg.photo.isSome: msg.photo.get[0].fileId else: "")
                #TODO: responses to others comments


proc on_tg_allow_c*(bot: Telebot, cmd: Command) {.async.} =
    if cmd.message.chat.kind != "private":
        var send_msg = cmd.message.chat.id.newMessage(ONLY_PRIVATE)
        bot.send(send_msg)
    else:
        var send_msg = cmd.message.chat.id.newMessage(TOS)
        send_msg.parseMode = "markdown"
        send_msg.replyMarkup = newReplyKeyboardMarkup(@[@[initKeyBoardButton(ACCEPT)],@[initKeyBoardButton(DECLINE)]])
        send_msg.replyMarkup.oneTimeKeyboard = option(true)
        bot.send(send_msg)

proc on_tg_allow_rc*(db: AsyncMysqlPool): CommandCallback =
    return proc (bot: Telebot, cmd: Command) {.async.} =
        #TODO
        discard
    
proc on_tg_disable_news*(db: AsyncMysqlPool, disable = true): CommandCallback =
    return proc (bot: Telebot, cmd: Command) {.async.} =
        let chat_id = cmd.message.chat.id
        if cmd.message.chat.kind == "private":
            bot.send(chat_id.newMessage(ONLY_PUBLIC))
        else:
            if cmd.message.chat.kind == "supergroup":
                let admins = await bot.getChatAdministrators($cmd.message.chat.id)
                var can = false
                for a in admins:
                    can = a.user.id == cmd.message.fromUser.get.id
                    if can: break
                if not can: return

            var send_msg: MessageObject
            if disable:
                asyncCheck db.set_active_tosend(chat_id, false)
                send_msg = chat_id.newMessage(DISABLE_NEWS)
            else:
                asyncCheck db.set_active_tosend(chat_id, true)
                send_msg = chat_id.newMessage(ENABLE_NEWS)
            bot.send(send_msg)

proc on_tg_start*(db: AsyncMysqlPool): CommandCallback =
    return proc (bot: Telebot, command: Command) {.async.} =
        discard await bot.send(newMessage(command.message.chat.id, "Añade este bot a un grupo para recibir y comentar las noticias de Tecnonucleous. \nSi desea recibirlas en privado, suscribase al canal @Tecnonucleous ."))

        let cmd = command.params.split(' ')
        if cmd.len() > 0 and cmd[0] != "":
            if cmd[0].toLower() == "accept":
                asyncCheck on_tg_allow_c(bot, command)
            elif cmd[0].isDigit() and (await db.search_article(cmd[0].parseInt())).len() > 0:
                to_comment[command.message.fromUser.get.id] = cmd[0].parseInt()

                var send_msg = command.message.chat.id.newMessage(SEND_COMMENT)
                #send_msg.replyMarkup = newForceReply(true)
                bot.send(send_msg)
            else:
                var send_msg = command.message.chat.id.newMessage("Esta funcion todavia no esta implementada, vuelve otro dia.")
                bot.send(send_msg)

proc tg_update*(priv_channel: string, db: AsyncMysqlPool): UpdateCallback =
    return proc (b: Telebot, up: Update) {.async.} =
        if up.message.isSome and up.message.get.fromUser.isSome and not await db.exist_user(up.message.get.fromUser.get.id):
            asyncCheck db.add_user(up.message.get.fromUser.get.id, up.message.get.fromUser.get.firstName)

        if (up.message.isSome() and up.message.get.chat.kind != "private") or (up.channelPost.isSome() and up.channelPost.get.chat.id != priv_channel.parseInt()):
            var msg: Message
            if up.message.isSome(): msg = up.message.get
            if up.channelPost.isSome(): msg = up.channelPost.get
            
            asyncCheck check_public_changes(db, b, msg)
            asyncCheck check_comments(db, b, msg)
        elif up.message.isSome() and up.message.get.chat.kind == "private":
            let msg = up.message.get

            if msg.forwardDate.isNone and msg.text.isSome() and msg.text.get.toLower() == ACCEPT:
                if not waitFor db.exist_accept(msg.fromUser.get.id):
                    asyncCheck db.add_accept(msg.fromUser.get.id, msg.messageId)
                    asyncCheck b.forwardMessage(priv_channel, msg.chat.id.intToStr(), msg.messageId)

                var send_msg = msg.chat.id.newMessage(ACCEPTED)
                #send_msg.replyMarkup = newReplyKeyboardRemove(false)
                b.send(send_msg)
            elif msg.forwardDate.isNone and msg.text.isSome() and msg.text.get.toLower() == DECLINE:
                if waitFor db.exist_user(msg.fromUser.get.id):
                    asyncCheck db.add_accept(msg.fromUser.get.id, 0)
                    asyncCheck b.forwardMessage(priv_channel, msg.chat.id.intToStr(), msg.messageId)

                var send_msg = msg.chat.id.newMessage(DECLINED)
                #send_msg.replyMarkup = newReplyKeyboardRemove(false)
                b.send(send_msg)
            elif msg.replyToMessage.isSome and msg.replyToMessage.get.text.get == SEND_COMMENT and to_comment.hasKey(msg.fromUser.get.id):
                discard await db.add_comments(msg.messageId, msg.fromUser.get.id, msg.chat.id, to_comment[msg.fromUser.get.id], if msg.text.isSome: msg.text.get else: "", if msg.photo.isSome: msg.photo.get[0].fileId else: "")
                if not waitFor db.exist_accept(msg.fromUser.get.id):
                    asyncCheck on_tg_allow_c(b, Command(message: msg, params: ""))

        if up.editedMessage.isSome() and up.editedMessage.get.chat.kind == "private":
            let msg = up.editedMessage.get
            if await db.exist_accept(msg.fromUser.get.id, msg.messageId):
                asyncCheck db.add_accept(msg.fromUser.get.id, 0)
                b.send(msg.chat.id.newMessage(DECLINED))

proc tg_inline*(db: AsyncMysqlPool): InlineQueryCallback =
    return proc (b: Telebot, query: InlineQuery) {.async.} =
        let err_msg = option(InputTextMessageContent(messageText: "error", parseMode: "markdown"))

        if query.query.match(re"[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)").isSome:
            let db_query = await db.search_article(-1, query.query)
            var results: seq[InlineQueryResultArticle]
            for db_result in db_query:
                echo db_result.title
                echo db_result.title.escapeJson()

                let resp_msg = option(InputTextMessageContent(messageText: "[" & db_result.title.unidecode() & "](" & db_result.url & ")", parseMode: "markdown"))
                let resp_key = option(gen_art_key($db_result.id, db_result.url))
                results.add(InlineQueryResultArticle(type: "article", id: $db_result.id,
                                                    title: db_result.title.unidecode(),
                                                    description: " ", #TODO: description
                                                    url: db_result.url,
                                                    hide_url: true,
                                                    thumb_url: option(db_result.img),
                                                    inputMessageContent: resp_msg,
                                                    replyMarkup: resp_key))

            if db_query.len > 0:
                discard await b.answerInlineQuery(query.id, results, 604800, false)
            else:
                discard await b.answerInlineQuery(query.id, @[InlineQueryResultArticle(type: "article", id: "invalidurl", title: INVALID_URL, description: INVALID_URL, url: "https://tecnonucleous.com/404/", hide_url: true, inputMessageContent: err_msg)], 86400, false)
        else:
            #TODO: Make a search? It can be tedious
            discard await b.answerInlineQuery(query.id, @[InlineQueryResultArticle(type: "article", id: "nourl", title: INSERT_URL, description: INSERT_URL, url: "https://tecnonucleous.com/404/", hide_url: true, inputMessageContent: err_msg)], 86400, false)

proc set_vars*(b: TeleBot) {.async.} =
    cache_me = await b.getMe()

proc tg_send*(chat_id: int, b: TeleBot, internal_id: string, msg: Webhook_msg): Future[Message] =
    var tg_msg = newMessage(chat_id, "[" & msg.post_title & "](" & msg.post_url & ")\n" & msg.post_description)
    tg_msg.parseMode = "markdown"
    tg_msg.replyMarkup = gen_art_key(internal_id, msg.post_url)
    return b.send(tg_msg)