import nre, httpclient

type
    Webhook_msg* = ref object
        text*: string
        icon*: string
        post_title*: string
        post_url*: string
        post_img*: string
        post_description*: string

template get_element(exp: string, page: string): string =
    #TODO: HTML entities
    let f_exp = re(exp)
    var p_match = page.find(f_exp)
    if p_match.isSome:
        p_match.get.captures().toSeq()[0]
    else:
        "Bad parser"

proc newWebhook_msg*(text: string, icon: string): Webhook_msg =
    new(result)
    result.text = text
    result.icon = icon

    let url = re"[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)"
    var match = text.find(url)
    if match.isSome:
        result.post_url = match.get.str
    
        let page = newHttpClient().getContent(result.post_url)
    
        result.post_title = get_element("<title>(.*)<\\/title>", page)
        result.post_img = get_element("<meta property=\"og:image\" content=\"(.*)\" \\/>", page)
        if result.post_img == "Bad parser": result.post_img = icon
        result.post_description = get_element("<meta property=\"og:description\" content=\"(.*)\" \\/>", page)