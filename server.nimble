# Package

version     = "0.1.0"
author      = "endes"
description = "Server for push notifications and telegram bot.."
license     = "AGPL v3.0"
srcDir      = "src"

# Deps

requires "telebot >= 0.5.0"
requires "httpbeast >= 0.2.1"
requires "cligen >= 0.9.17"
requires "nim >= 0.18.0"